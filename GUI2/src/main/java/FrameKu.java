
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;


/**
 *
 * @author Microsoft User
 */
public class FrameKu extends JFrame {
    public FrameKu(){
    this.setSize(300, 500);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setTitle("Ini Class Turunan dari Class JFrame");
    
    JPanel panel = new JPanel();
    JButton tombol = new JButton();
    JLabel label = new JLabel();
    JTextField teks = new JTextField(20);
    JCheckBox cek = new JCheckBox();
    JRadioButton radBut = new JRadioButton();
    tombol.setText("Ini Tombol");
    label.setText("Ini Label");
    cek.setText("Aw Aw Check");
    radBut.setText("Ini Radio Button");
    panel.add(tombol);
    panel.add(label);
    panel.add(teks);
    panel.add(cek);
    panel.add(radBut);
    this.add(panel);
    this.setVisible(true);
    }
    
    public static void main(String[] args) {
        new FrameKu();
    }
}
